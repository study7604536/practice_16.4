﻿// Practice_16.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <time.h>

int main()
{
    using namespace std;
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int N = 12;
    int Array[N][N];
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            Array[i][j] = i + j;
            cout << Array[i][j]<<" ";
        }
        cout << "\n";
        cout << "\n";
    }

    int Date_index = buf.tm_mday % N;
    cout << "Today is " << buf.tm_mday<<"\n";

    int Summa (0);

    for (int j = 0; j < N; ++j)
    {
        Summa += Array[Date_index][j];
    }
    cout <<"Summ of aray string number  " << Date_index << " is " << Summa << "\n";
}

